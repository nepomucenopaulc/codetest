using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OData.Edm;
using System.Linq;
using TestCode.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace TestCode
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(mvcOptions =>
                mvcOptions.EnableEndpointRouting = false);

            //This Line Connects segment connects the context to the database using the connection string specified on the application.json
            services.AddOData();
            string CodeTestConnection = Configuration.GetConnectionString("CodeTestConnection");
            services.AddDbContext<CodeTestContext>(
                 options => options.UseSqlServer(CodeTestConnection));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseMvc(routeBuilder =>
            {
                //Enabled odata functions.
                routeBuilder.Count().Filter().OrderBy().Expand().Select().MaxTop(null);
                routeBuilder.MapODataServiceRoute("odata", "odata", GetEdmModel());
            });
        }
        IEdmModel GetEdmModel()
        {
            //Specify the End Points Included in Routing This Basically means that Model "Account" = Odata Route "Accounts"
            var odataBuilder = new ODataConventionModelBuilder();
            odataBuilder.EntitySet<Account>("Accounts");
            odataBuilder.EntitySet<Payment>("Payments");
            odataBuilder.EntitySet<Status>("Statuses");
            return odataBuilder.GetEdmModel();
        }
    }
}
